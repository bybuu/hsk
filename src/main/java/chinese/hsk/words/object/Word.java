package chinese.hsk.words.object;

import lombok.Getter;

public abstract class Word {
	
//	@Getter
	private String word;
	
	Word(final String word) {
		this.word = word;
	}
	
	public String getWord() {
		return this.word;
	}
}
