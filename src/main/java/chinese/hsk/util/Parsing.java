package chinese.hsk.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import chinese.hsk.ConstantSize1;
import chinese.hsk.ConstantSize2;
import chinese.hsk.ConstantSize3;
import chinese.hsk.ConstantSize4;
import chinese.hsk.words.object.Unranked;
import chinese.hsk.words.object.Word;
import lombok.extern.slf4j.Slf4j;


public class Parsing {
	
	public Parsing() {
		
	}
	
	public List<Word> parsingAll(String textToParse) {
		List<Word> parsedList = new ArrayList<Word>();
		parsedList.add(new Unranked(textToParse));
		return parsingN(parsedList,4);
	}
	
	private List<Word> parsingN(List<Word> parsedList,int sizeMax) {
		Word[][] listword = new Word[sizeMax][];
		listword[0] = ConstantSize1.oneCharacters;
		listword[1] = ConstantSize2.twoCharacters;
		listword[2] = ConstantSize3.threeCharacters;
		listword[3] = ConstantSize4.fourCharacters;
		int n;
		for (int k = 3;k>=0;k--) {
			int j = 0;
			List<Word> newParsedList = new ArrayList<Word>();
			while (j < parsedList.size()) {
				Word temp = parsedList.get(j);
				int i= 0;
				int last_index = 0; 
				if (temp.getClass() == Unranked.class) {
					n = temp.getWord().length();
					while (n > k && i < n - k) {
						String wordCompare = temp.getWord().substring(i, i+k+1);
						for(Word word: listword[k]) {
							if (wordCompare.equals(word.getWord())) {
								if (i != last_index) {
									newParsedList.add(new Unranked(temp.getWord().substring(last_index, i)));
								}
								last_index = i+k+1;
								newParsedList.add(word);
								break;
							}
						}
						i++;
					}
					if (last_index < n) {
						newParsedList.add(new Unranked(temp.getWord().substring(last_index, n)));
					}
				} else {
					newParsedList.add(temp);
				}
				j++;
			}
			parsedList = newParsedList;
		}
		return parsedList;
	}
	
	
}
